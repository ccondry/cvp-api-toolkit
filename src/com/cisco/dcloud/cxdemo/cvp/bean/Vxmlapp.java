package com.cisco.dcloud.cxdemo.cvp.bean;

import java.io.InputStream;
import java.util.Iterator;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.json.JSONObject;

import com.cisco.dcloud.cxdemo.cvp.annotation.RestPath;
import com.cisco.dcloud.cxdemo.cvp.CvpRestClient;

@RestPath("vxmlapp")
public class Vxmlapp extends CvpRestClient {
	private static final String BASE_PATH = "/vxmlapp";
	private static final String VXMLAPP_DEPLOY = "/vxmlapp/deploy";
	private static final String GET_VXMLAPP = "/vxmlapp/{name}";

	public Vxmlapp(String restServer, String restUsername, String restPassword, boolean restIgnoreSsl) {
		super(restServer, restUsername, restPassword, restIgnoreSsl);
	}

	public Response get( String name ) {
		// put the name parameter into the URL
		Response response = target.path( GET_VXMLAPP )
				.resolveTemplate( "name", name )
				.request( MediaType.APPLICATION_JSON_TYPE )
				.get( Response.class );
		return response;
	}

	public JSONObject delete( String name ) {
		// put the name parameter into the URL
		Response response = target.path( GET_VXMLAPP )
				.resolveTemplate( "name", name )
				.request( MediaType.APPLICATION_JSON_TYPE )
				.delete( Response.class );
		return new JSONObject(response.readEntity(String.class));
	}

	public Response deploy(InputStream stream) {
		FormDataMultiPart part = new FormDataMultiPart();
		part.field("vxmlapp", stream, MediaType.TEXT_PLAIN_TYPE);
		JSONObject config = new JSONObject();
		// base request string container
		// no other parameters are required to deploy to all servers 
		config.put( "vxmlapp", "" );
		part.field( "vxmlDeployConfig", config.toString() );

		Response response = target.path( VXMLAPP_DEPLOY )
				.request( MediaType.APPLICATION_JSON_TYPE )
				.post( Entity.entity( part, MediaType.MULTIPART_FORM_DATA ), Response.class );
		return response;
	}

	/*
	 * From the API documentation, https://developer.cisco.com/fileMedia/download/7865fc47-db28-469c-b87c-c0e71b67d960
	 * 
The following query parameters are specified using (q=) syntax.
Name : appname
Description: Text parameter with the application name. The value can be
wildcarded.
Required : No.
Validations : Should not contain any starting wildcard characters.
Notes : The following parameters are specified as separate URL query
parameters with "&" separators.

Name : summary
Description: Text parameter to indicate whether only application name is
listed or full details to be listed.
Required : No.
Values : true
Validations : Only allowed values.
Notes : URL query parameter. Default is false.

Name : startIndex
Description: Starting index of the result list.
Required : No.
Values : Zero-based: 0 is the first element. DEFAULT = 0.
Validations : zero or more.
Notes : URL query parameter.

Name : resultsPerPage
Description: Specifies number of elements to retrieve.
Required : No.
Values : MIN=1, DEFAULT=25, MAX=50
Validations : One or more.
Notes: URL query parameter
	 * 
	 */
	public Response list() {
		return list( new JSONObject() );
	}

	public Response list( JSONObject params ) {
		// add all 	params as URL query parameters
		Iterator<?> keys = params.keys();
		while( keys.hasNext() ) {
			String key = ( String ) keys.next();
			Object value = params.get( key );
			target = target.queryParam( key, value );
		}

		Response response = target.path( BASE_PATH )
				.request( MediaType.APPLICATION_JSON_TYPE )
				.get( Response.class );
		return response;
	}
}
